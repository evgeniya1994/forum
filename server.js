var express 	= require('express'), 
	http 		= require("http"), 
	body_parser = require("body-parser"),
	app 		= express()
///config///
var port = 3000;
var pathSeparator = (__dirname.indexOf("\\") != -1) ? "\\" : "/";
var dir = __dirname+pathSeparator+'static';
///config///
app.listen(port);
console.log("SITE: started on " + port);
app.post('/api/sign_up',function(req,res){
	console.log(req);
	res.send('ok');
});
app.use(express.static(dir));
app.get('/*',function(req,res){
	res.sendFile(dir + '/login.html');
});